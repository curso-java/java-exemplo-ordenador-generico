package utfpr.ct.dainf.exemplo;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * Comparador entre beans que permite escolher dinamicamente a propriedade que
 * será usada na comparação. A propriedade desejada pode, inclusive, ser uma
 * subpropriedade. O nome da propriedade deve ser especificado de acordo com
 * o padrão definido pela classe <code>org.apache.commons.beanutils.PropertyUtilsBean</code>.
 * @param <T> O tipo de dado a ser comparado
 * @see org.apache.commons.beanutils.BeanUtilsBean
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class BeanComparator<T> implements Comparator<T> {

    private String property;
    private boolean asc;

    /**
     * Cria um ordenador que ordena pela propriedade {@code property}.
     * Se {@code asc} for {@code true} ordena em ordem anscendente, caso
     * contrário, em ordem descendente.
     * @param property A propriedade pela qual desejamos ordenar
     * @param asc Se {@code true} ordena em ordem ascendente
     */
    public BeanComparator(String property, boolean asc) {
        this.property = property;
        this.asc = asc;
    }

    /**
     * Cria um ordenador que ordena pela propriedade {@code property} em ordem
     * ascendente.
     * @param property A propriedade pela qual desejamos ordenar
     */
    public BeanComparator(String property) {
        this(property, true);
    }

    /**
     * Retorna o nome da propriedade pela qual desejamos ordenar.
     * @return O nome da propriedade
     */
    public String getProperty() {
        return property;
    }

    /**
     * Atribui o nome da propriedade pela qual desejamos ordenar.
     * @param property O nome da propriedade
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * Retorna {@code true} se o critério de ordenação for ascendente.
     * @return O status do critério de ordenação
     */
    public boolean isAsc() {
        return asc;
    }

    /**
     * Atribui o status do critério de ordenação ascendente.
     * Um valor {@code true} indica ordenação ascendente.
     * @param asc O status do critério de ordenação
     */
    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    /**
     * Compara o valor de duas propriedades que devem implementar a interface
     * <code>Comparable</code> usando o método <code>compareTo</code>.
     * Este método leva em consideração a possibilidade de algum dos valores
     * ser nulo e, neste caso, considera o valor nulo igual ao valor nulo e o
     * valor nulo menor que o não-nulo retornando valores apropriados.
     * @param <C> O tipo do comparável
     * @param p1 O valor da primeira propriedade
     * @param p2 O valor da segunda propriedade
     * @param asc <code>true</code> se a ordernação deve ser ascendente
     * @return O resultado do método {@code compareTo}
     */
    public static <C extends Comparable> int compareProps(C p1, C p2, boolean asc) {
        if (p1 == null && p2 == null)
            return 0;
        else if (p1 == null && p2 != null) // p1 < p2
            return asc ? -1 : 1;
        else if (p1 != null && p2 == null) // p1 > p2
            return asc ? 1 : -1;
        return asc ? p1.compareTo(p2) : p2.compareTo(p1);
    }

    /**
     * Compara dois valores (b1 e b2) de uma propriedade de um bean.
     * Retorna: 0, se b1 == b2
     *          negativo, se b1 &lt; b2
     *          positivo, se b1 &gt; b2
     * @param b1 O valor da primeira propriedade
     * @param b2 O valor da segunda propriedade
     * @return O resultado da comparação
     */
    @Override
    public int compare(T b1, T b2) {
        Comparable<T> c1 = null, c2 = null;
        try {
            try {
                c1 = b1 == null ? null : (Comparable)PropertyUtils.getNestedProperty(b1, property);
            } catch (NestedNullException e) { // quando uma propriedade intermediária é nula
                c1 = null;
            }
            try {
                c2 = b2 == null ? null : (Comparable)PropertyUtils.getNestedProperty(b2, property);
            } catch (NestedNullException e) { // quando uma propriedade intermediária é nula
                c2 = null;
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        return compareProps(c1, c2, asc);
    }
}
