package utfpr.ct.dainf.exemplo;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Implementa diversos métodos estáticos para ordenação de estruturas de dados.
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class SortUtils {

    /**
     * Ordena uma lista em ordem ascendente ou descendente pelo valor de uma
     * propriedade. A propridade deve ser especificada utilizando o padrão de
     * JavaBeans, por exemplo, <i>pessoa.cpf.numero</i>, e cada propriedade
     * no caminho deve definir o respectivo <i>getter</i>.
     * @param <T> A classe dos elementos contidos na lista.
     * @param list A lista a ser ordenada.
     * @param property A propriedade usada como critério de ordenação.
     * @param asc <code>true</code> se a ordenação for ascendente
     * @return A mesma lista ordenada.
     */
    public static <T> List<? extends T> sort(List<? extends T> list, final String property, final boolean asc) {
        if (list.isEmpty()) {
            return list;
        }
        final BeanComparator<T> comparator = new BeanComparator<>(property, asc);
        try {
            Collections.sort(list, new Comparator<T>() {
                @Override
                public int compare(T e1, T e2) {
                    return comparator.compare(e1, e2);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    /**
     * Ordena uma lista em ordem ascendente pelo valor de uma
     * propriedade. A propridade deve ser especificada utilizando o padrão de
     * JavaBeans, por exemplo, <i>pessoa.cpf.numero</i>, e cada propriedade
     * no caminho deve definir o respectivo <i>getter</i>.
     * @param <T> A classe dos elementos contidos na lista.
     * @param list A lista a ser ordenada.
     * @param property A propriedade usada como critério de ordenação.
     * @return A mesma lista ordenada.
     */
    public static <T> List<? extends T> sort(List<? extends T> list, String property) {
        return sort(list, property, true);
    }
}
