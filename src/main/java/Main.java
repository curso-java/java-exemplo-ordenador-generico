
import java.util.ArrayList;
import utfpr.ct.dainf.exemplo.Jogador;
import utfpr.ct.dainf.exemplo.SortUtils;


/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Jogador> time = new ArrayList<>();

        time.add(new Jogador(4, "Ciclano"));
        time.add(new Jogador(1, "Fulano"));
        time.add(new Jogador(10, "Beltrano"));
        time.add(new Jogador(10, "Mário"));
        time.add(new Jogador(1, "Beltrano"));
        time.add(new Jogador(7, "José"));

        System.out.println("ORDENADO POR NÚMERO ASCENDENTE");
        SortUtils.sort(time, "numero");
        String format = "%3d %-20s";
        System.out.println("Num Nome");
        for (Jogador j: time) {
            System.out.println(String.format(format, j.getNumero(), j.getNome()));
        }

        System.out.println("ORDENADO POR NOME DESCENDENTE");
        SortUtils.sort(time, "nome", false);
        System.out.println("Num Nome");
        for (Jogador j: time) {
            System.out.println(String.format(format, j.getNumero(), j.getNome()));
        }
    }
}
